import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatDatepickerModule } from '@angular/material/datepicker';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MatSelectCountryModule } from '@angular-material-extensions/select-country';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ChartComponent } from './chart/chart.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CountrySelectionComponent } from './country-selection/country-selection.component';
import { MAT_DATE_LOCALE, MatNativeDateModule } from '@angular/material/core';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { ChartOptionsComponent } from './chart/chart-options/chart-options.component';
import { PatronComponent } from './patron/patron.component';

@NgModule({
  declarations: [
    AppComponent,
    ChartComponent,
    CountrySelectionComponent,
    ChartOptionsComponent,
    PatronComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule, 
    MatSelectCountryModule,
    MatDatepickerModule,
    MatAutocompleteModule,
    MatNativeDateModule,
    BrowserAnimationsModule
  ],
  providers: [ {provide: MAT_DATE_LOCALE, useValue: 'es-ES'}],
  bootstrap: [AppComponent]
})
export class AppModule { }
