import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Country, MatSelectCountryComponent } from '@angular-material-extensions/select-country';
import { ChartBusService } from '../services/chart-bus.service';
import { distinctUntilChanged } from 'rxjs/operators';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-country-selection',
  templateUrl: './country-selection.component.html',
  styleUrls: ['./country-selection.component.scss']
})
export class CountrySelectionComponent implements OnInit {

  @ViewChild('country1', {static:true}) country1: MatSelectCountryComponent;
  @ViewChild('country2', {static:true}) country2: MatSelectCountryComponent;
  @ViewChild('country3', {static:true}) country3: MatSelectCountryComponent;
  @ViewChild('country4', {static:true}) country4: MatSelectCountryComponent;
  @ViewChild('country5', {static:true}) country5: MatSelectCountryComponent;
  @ViewChild('country6', {static:true}) country6: MatSelectCountryComponent;
  @ViewChild('country7', {static:true}) country7: MatSelectCountryComponent;

  offset2: FormControl;
  offset3: FormControl;
  offset4: FormControl;
  offset5: FormControl;
  offset6: FormControl;
  offset7: FormControl;

  showCountry: boolean[] = [false, false, false, false, false, false];
  
  constructor(private chartBus: ChartBusService) { }

  ngOnInit(): void {
    this.offset2 = new FormControl();
    this.offset3 = new FormControl();
    this.offset4 = new FormControl();
    this.offset5 = new FormControl();
    this.offset6 = new FormControl();
    this.offset7 = new FormControl();

    
    //this.chartBus.updateOffset(7, this.offset7.value);
    

    // Initial values
    let initialValue1 = localStorage.getItem('covid19.country.0.selected.alpha2Code');
    if(initialValue1 == null) {
      initialValue1 = 'ES';
    }
    if(initialValue1) {
      let country1 = this.country1.countries.find(c => c.alpha2Code == initialValue1);
      this.country1.selectedCountry = country1;
      
      this.chartBus.updateCountry(0, country1.alpha2Code);
    }


    let initialValue2 = localStorage.getItem('covid19.country.1.selected.alpha2Code');
    if(initialValue2 == null) {
      initialValue2 = 'FR';
      this.offset2.setValue(4);
    }
    if(initialValue2) {
      let country2 = this.country2.countries.find(c => c.alpha2Code == initialValue2);
      this.country2.selectedCountry = country2;
      this.showCountry[0] = true;

      this.chartBus.updateCountry(1, country2.alpha2Code);
    }
    let initialOffset2 = localStorage.getItem('covid19.country.1.selected.offset');
    if(initialOffset2) {
      this.offset2.setValue(+initialOffset2);
    }


    let initialValue3 = localStorage.getItem('covid19.country.2.selected.alpha2Code');
    if(initialValue3 == null) {
      initialValue3 = 'IT';
      this.offset3.setValue(-6);
    }
    if(initialValue3) {
      let country3 = this.country3.countries.find(c => c.alpha2Code == initialValue3);
      this.country3.selectedCountry = country3;
      this.showCountry[1] = true;

      this.chartBus.updateCountry(2, country3.alpha2Code);
    }
    let initialOffset3 = localStorage.getItem('covid19.country.2.selected.offset');
    if(initialOffset3) {
      this.offset3.setValue(+initialOffset3);
    }



    let initialValue4 = localStorage.getItem('covid19.country.3.selected.alpha2Code');
    if(initialValue4 == null) {
      //initialValue4 = 'GB';
      //this.offset4.setValue(10);
    }
    if(initialValue4) {
      let country4 = this.country4.countries.find(c => c.alpha2Code == initialValue4);
      this.country4.selectedCountry = country4;
      this.showCountry[2] = true;

      this.chartBus.updateCountry(3, country4.alpha2Code);
    }
    let initialOffset4 = localStorage.getItem('covid19.country.3.selected.offset');
    if(initialOffset4) {
      this.offset4.setValue(+initialOffset4);
    }


    let initialValue5 = localStorage.getItem('covid19.country.4.selected.alpha2Code');
    if(initialValue5 == null) {
      //initialValue5 = 'US';
      //this.offset5.setValue(5);
    }
    if(initialValue5) {
      let country5 = this.country5.countries.find(c => c.alpha2Code == initialValue5);
      this.country5.selectedCountry = country5;
      this.showCountry[3] = true;

      this.chartBus.updateCountry(4, country5.alpha2Code);
    }
    let initialOffset5 = localStorage.getItem('covid19.country.4.selected.offset');
    if(initialOffset5) {
      this.offset5.setValue(+initialOffset5);
    }


    let initialValue6 = localStorage.getItem('covid19.country.5.selected.alpha2Code');
    if(initialValue6 == null) {
      // initialValue6 = 'KR';
      // this.offset6.setValue(-9);
    }
    if(initialValue6) {
      let country6 = this.country6.countries.find(c => c.alpha2Code == initialValue6);
      this.country6.selectedCountry = country6;
      this.showCountry[4] = true;

      this.chartBus.updateCountry(5, country6.alpha2Code);
    }
    let initialOffset6 = localStorage.getItem('covid19.country.5.selected.offset');
    if(initialOffset6) {
      this.offset6.setValue(+initialOffset6);
    }


    let initialValue7 = localStorage.getItem('covid19.country.6.selected.alpha2Code');
    if(initialValue7) {
      let country7 = this.country7.countries.find(c => c.alpha2Code == initialValue7);
      this.country7.selectedCountry = country7;
      this.showCountry[5] = true;

      this.chartBus.updateCountry(6, country7.alpha2Code);
    }
    let initialOffset7 = localStorage.getItem('covid19.country.6.selected.offset');
    if(initialOffset7) {
      this.offset7.setValue(+initialOffset7);
    }


    
    this.offset2.valueChanges.pipe(distinctUntilChanged()).subscribe(() => {
      this.onOffsetChange(this.offset2.value ? this.offset2.value : 0, 1);
    });
    this.chartBus.updateOffset(2, this.offset2.value ? this.offset2.value : 0);

    this.offset3.valueChanges.pipe(distinctUntilChanged()).subscribe(() => {
      this.onOffsetChange(this.offset3.value ? this.offset3.value : 0, 2);
    });
    this.chartBus.updateOffset(3, this.offset3.value ? this.offset3.value : 0);

    this.offset4.valueChanges.pipe(distinctUntilChanged()).subscribe(() => {
      this.onOffsetChange(this.offset4.value ? this.offset4.value : 0, 3);
    });
    this.chartBus.updateOffset(4, this.offset4.value ? this.offset4.value : 0);

    this.offset5.valueChanges.pipe(distinctUntilChanged()).subscribe(() => {
      this.onOffsetChange(this.offset5.value ? this.offset5.value : 0, 4);
    });
    this.chartBus.updateOffset(5, this.offset5.value ? this.offset5.value : 0);

    this.offset6.valueChanges.pipe(distinctUntilChanged()).subscribe(() => {
      this.onOffsetChange(this.offset6.value ? this.offset6.value : 0, 5);
    });
    this.chartBus.updateOffset(6, this.offset6.value ? this.offset6.value : 0);

    this.offset7.valueChanges.pipe(distinctUntilChanged()).subscribe(() => {
      this.onOffsetChange(this.offset7.value ? this.offset7.value : 0, 6);
    });

  }

  onCountrySelected($event: Country, pos: number) {
    this.chartBus.updateCountry(pos, $event.alpha2Code);
    localStorage.setItem('covid19.country.'+pos.toString()+'.selected.alpha2Code', $event.alpha2Code);
  }

  onOffsetChange(offset: number, pos: number) {
    this.chartBus.updateOffset(pos+1, offset);
    localStorage.setItem('covid19.country.'+pos.toString()+'.selected.offset', offset.toString());
  }

  removeCountry(pos: number) {
    if(pos == 0) {
      this.country1.selectedCountry = null;
    } 
    if(pos == 1) {
      this.country2.selectedCountry = null;
      this.offset2.setValue(null);
    }
    if(pos == 2) {
      this.country3.selectedCountry = null;
      this.offset3.setValue(null);
    }
    if(pos == 3) {
      this.country4.selectedCountry = null;
      this.offset4.setValue(null);
    }
    if(pos == 4) {
      this.country5.selectedCountry = null;
      this.offset5.setValue(null);
    }
    if(pos == 5) {
      this.country6.selectedCountry = null;
      this.offset6.setValue(null);
    }
    if(pos == 6) {
      this.country7.selectedCountry = null;
      this.offset7.setValue(null);
    }

    this.showCountry[pos - 1] = false;

    this.chartBus.updateCountry(pos, null);
    localStorage.setItem('covid19.country.'+pos.toString()+'.selected.alpha2Code', '');
    localStorage.setItem('covid19.country.'+pos.toString()+'.selected.offset', '');
  }

 showMoreCountries($event) {
    $event.preventDefault();

    for(let i = 0; i < this.showCountry.length; i++) {
      if(!this.showCountry[i]) {
        this.showCountry[i] = true;
        return;
      }
    }    
  }

  hasMoreCountries(){
    return this.showCountry.some(c => !c)
  }
}

