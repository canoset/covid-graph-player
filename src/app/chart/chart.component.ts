import { Component, OnInit, OnDestroy } from '@angular/core';
import { EudataService } from '../services/eudata.service';
import { ChartBusService } from '../services/chart-bus.service';
import { ChartConfig } from '../model/chart.config';
import { Eudata } from '../model/eudata';
import { Subscription } from 'rxjs';

declare var google: any; 

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss'], 
  inputs: ['chartId']
})
export class ChartComponent implements OnInit, OnDestroy {
  config: ChartConfig;
  data;
  chartId: string;

  googleChartIsReady: boolean;
  loadingData: boolean;
  preparingData: boolean;

  sub: Subscription;
  
  constructor(private eudataService: EudataService, private busService: ChartBusService) { }
  
  ngOnInit(): void {
    this.busService.addChart(this.chartId);

    this.sub = this.busService.busSource$.subscribe(()=> {
      this.config = this.busService.getConfig(this.chartId);
      this.init();
    });

    setTimeout(() => {
      // Load the Visualization API and the corechart package.
      google.charts.load('current', {'packages':['corechart']});
      // Set a callback to run when the Google Visualization API is loaded.
      google.charts.setOnLoadCallback(() => this.GoogleChartIsReady());
    });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  private GoogleChartIsReady() {
    console.log("Google chart is ready");    
    this.googleChartIsReady = true;
    this.busService.googleReady();
  }

  init() {
    if(!this.googleChartIsReady) {
      console.log("Stopping google is not ready")
      return;
    }

    if(!this.config.countries || this.config.countries.length == 0) {
      console.log("Stopping no countries")
      return;
    }

    this.loadingData = true;
    this.loadData().then((d) => {
      this.loadingData = false;

      this.preparingData = true;
      this.data = this.fromMapToGoogleChartData(d);
      this.preparingData = false;
      this.drawChart();
    });
  }

  private fromMapToGoogleChartData(raw: Map<string, Eudata[]>) {
    // Ordenación de claves
    let days = Object.keys(raw).sort();
    
    // fill missing dates
    const missingDays = this.getMissingDates(days);
    days = [...days, ...missingDays].sort();

    // Eliminación fechas anteriores y posteriores al rango  
    days = days.filter(d => new Date(d) > this.config.start_date);

    if(this.config.end_date) {
      days = days.filter(d => new Date(d) <= this.config.end_date);
    }

    // Ampliación eje x (si hay offset)
    days = [
      ...days, 
      ...this.extendXaxis(days[days.length-1])
    ];

    // Extract info day by day
    let data = days.map((date, i) => {
      let aux = this.buildMetrics(raw, date);

      // x-axis is in number of days, not dates
      return [(i+1), this.htmlForNotes(aux.countries, aux.dates, aux.values), ...aux.values];
    });

    return data;
  }

  private htmlForNotes(countries: string[], dates: string[], values: number[]): string {
    let html = `<div>
        <table class="table">
          <thead>
          <tr>
            <th scope="col">Country</th>
            <th scope="col">Date</th>
            <th scope="col">Value</th>
          </tr>
        </thead>
        <tbody>`;
    for(let i = 0; i<dates.length; i++) {
      html += 
        `<tr>
          <td>
            <img src="/assets/svg-country-flags/svg/${countries[i].toLowerCase()}.svg" width="20" alt="${countries[i]} "/>
          </td>
          <td class="text-nowrap">${dates[i]}</td>
          <td><b>${values[i] || '-'}</b></td>
        </tr>`;
    }
    html += `
        </tbody>
        </table>
      </div>`;
    return html;
  }


  private findMetric(raw: Map<string, Eudata[]>, date: any, country: string, metric): number {
    const v: Eudata = raw[date] ? raw[date].find(v => v.geoId == country) || null : null;
    
    if(v) {
      if(metric == 'new_cases'){
        return v.new_cases;
      } 
      else if(metric == 'new_deaths') {
        return v.new_deaths;
      } 
      else if(metric == 'total_deaths') {
        return v.total_deaths;
      } 
      else if(metric == 'total_cases') {
        return v.total_cases;
      }
      else if(metric == 'variation_cases1D') {
        return v.variation_cases1D;
      }
      else if(metric == 'variation_deaths1D') {
        return v.variation_deaths1D;
      }
      else if(metric == 'variation_cases2D') {
        return v.variation_cases2D;
      }
      else if(metric == 'variation_deaths2D') {
        return v.variation_deaths2D;
      }
      else if(metric == 'variation_cases4D') {
        return v.variation_cases4D;
      }
      else if(metric == 'variation_deaths4D') {
        return v.variation_deaths4D;
      }
      else if(metric == 'variation_cases7D') {
        return v.variation_cases7D;
      }
      else if(metric == 'variation_deaths7D') {
        return v.variation_deaths7D;
      }
      else if(metric == 'variation_cases14D') {
        return v.variation_cases14D;
      }
      else if(metric == 'variation_deaths14D') {
        return v.variation_deaths14D;
      }
      else if(metric == 'total_cases_per_1m') {
        return v.total_cases_per_1Mpop;
      }
      else if(metric == 'total_deaths_per_1m') {
        return v.total_deaths_per_1Mpop;
      }
      else if(metric == 'lethality') {
        return v.lethality;
      }
    }
    else 
      return null;
    
  }
  
  private async loadData(): Promise<Map<string, Eudata[]>> {
    const countries = this.config.countries.filter(v => v);
    return await this.eudataService.findByCountries(countries).toPromise();
  }

  drawChart() {
    // Create the data table.
    var data = new google.visualization.DataTable();

    // Día
    data.addColumn("number");

    // notas
    data.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});

    if(this.config.countries[0])
      data.addColumn("number", this.config.countries[0]+" "+this.config.metric);
    if(this.config.countries[1])
      data.addColumn("number", this.config.countries[1]+" "+this.config.metric);
    if(this.config.countries[2])
      data.addColumn("number", this.config.countries[2]+" "+this.config.metric);
    if(this.config.countries[3])
      data.addColumn("number", this.config.countries[3]+" "+this.config.metric);
    if(this.config.countries[4])
      data.addColumn("number", this.config.countries[4]+" "+this.config.metric);
    if(this.config.countries[5])
      data.addColumn("number", this.config.countries[5]+" "+this.config.metric);
    if(this.config.countries[6])
      data.addColumn("number", this.config.countries[6]+" "+this.config.metric);


    data.addRows(this.data);

    const options = this.optionsOfChart();
    const chart = this.typeOfChart();
    chart.draw(data, options);
  }

  private optionsOfChart() {

    let hAxis = {
      minValue: 0, 
      maxValue: 15
    }; // , textPosition: 'none';

    let vAxis: any = {
      minValue: 0, 
      maxValue: 15, 
    };

    if(!this.isPercentMetric()) {
      vAxis = {
        ...vAxis, 
        viewWindow: {
          min: 0
        }
      }
    }
    else {
      vAxis = {
        ...vAxis, 
        format: '#\'%\''
      }
    }



    // curveType: 'function',
    /* 
    */

    let mainOptions = {
      legend:'bottom',
      height: 400,      
      hAxis: hAxis,
      vAxis: vAxis,
      chartArea: {
        top:'5%', height:'75%', 
        left: '50', width:'85%'
      },
      trendlines: this.createTrendLIneConfig(),

      pointSize: this.getPointSize(),

      // This line makes the entire category's tooltip active.
      focusTarget: 'category',
      tooltip: { isHtml: true }
    };

    switch(this.config.chart_type) {
      case 'scatter' :
        return {
          ...mainOptions
        }
      case 'lines':
        return {
          ...mainOptions
        }
      case 'bars': 
        return {
          ...mainOptions
        }
      case 'columns':
        return {
          ...mainOptions
        }
      case 'area':
        return {
          ...mainOptions
        }
    }
  }


  private getPointSize() {
    switch(this.config.chart_type) {
      case 'scatter' :
        return 3;
      case 'lines':
      case 'columns': 
      case 'bars': 
      case 'area': 
        return 0;
    }
  }

  private createTrendLIneConfig() {
    const trendConfig = {
      type: this.getTrendType(),
      degree: this.getTrendDegree(),
      showR2: false,
      visibleInLegend: false, 
      labelInLegend: 'Tendency',
    };

    if(!this.config.trendline) {
      return {};
    }
    else {
      let obj = {};
      for(let i = 0; i < this.config.countries.length; i++) {
        if(i == 0) obj = Object.assign(obj, {0: trendConfig});
        if(i == 1) obj = Object.assign(obj, {1: trendConfig});
        if(i == 2) obj = Object.assign(obj, {2: trendConfig});
        if(i == 3) obj = Object.assign(obj, {3: trendConfig});
        if(i == 4) obj = Object.assign(obj, {4: trendConfig});
        if(i == 5) obj = Object.assign(obj, {5: trendConfig});
        if(i == 6) obj = Object.assign(obj, {6: trendConfig});
        if(i == 7) obj = Object.assign(obj, {7: trendConfig});
      }
      return obj;
    }    
  }

  private getTrendType(): string {
    if(this.config.trendline == 'lineal') 
      return 'lineal';
    else if(this.config.trendline == 'exp') 
      return 'exponential';
    else if(this.config.trendline.indexOf('poly') != -1) 
      return 'polynomial';
    else {
      return 'lineal';
    }
  }

  private getTrendDegree(): number {
    if(this.config.trendline.indexOf('poly') != -1)  {
      if(this.config.trendline.indexOf('poly2') != -1)  {
        return 2;
      }
      else if(this.config.trendline.indexOf('poly3') != -1)  {
        return 3;
      }
      else if(this.config.trendline.indexOf('poly4') != -1)  {
        return 4;
      }
      else if(this.config.trendline.indexOf('poly5') != -1)  {
        return 5;
      }
      else if(this.config.trendline.indexOf('poly6') != -1)  {
        return 6;
      }
      else if(this.config.trendline.indexOf('poly7') != -1)  {
        return 7;
      }
    }
    else return null;
  }

  private typeOfChart() {
    switch(this.config.chart_type) {
      case 'scatter' :
        return new google.visualization.ScatterChart(document.getElementById(this.getChartId()));
      case 'lines':
        return new google.visualization.LineChart(document.getElementById(this.getChartId()));
      case 'columns': 
        return new google.visualization.ColumnChart(document.getElementById(this.getChartId()));
      case 'bars': 
        return new google.visualization.BarChart(document.getElementById(this.getChartId()));
      case 'area': 
        return new google.visualization.AreaChart(document.getElementById(this.getChartId()));
    }
  }

  private getChartId(): string {
    return 'chart_div__'+this.chartId;
  }

  private calculateOffset(date: string, offset): string {
    if(offset) {
      let offset_date = new Date(date);
      offset_date.setDate(offset_date.getDate() + offset);
      return this.formatDate(offset_date);
    }
    else {
      return date;
    }
  }

  private formatDate(date: Date): string {
    return date.getFullYear() + '-' 
            + ('0' + (date.getMonth()+1)).slice(-2) + '-'
            + ('0' + date.getDate()).slice(-2) + '';
  }

  private extendXaxis(lastDate: string): string[] {
    const offsets = this.config.offsets.filter(o => o);
    const minOffset = (offsets.length) ? Math.min(...offsets) : 0;

    let newKeys: string[] = [];
    for(let i = 1; i < Math.abs(minOffset); i++) {
      let aux = new Date(lastDate);
      aux.setDate(aux.getDate() + i);
      newKeys.push(this.formatDate(aux));
    }
    return newKeys;
  }

  private buildMetrics(raw: Map<string, Eudata[]>, date: string): any {
    const first = this.findMetric(raw, date, this.config.countries[0], this.config.metric);
    let values = [];
    let countries = [];
    let dates = [];

    values.push(first);
    countries.push(this.config.countries[0]);
    dates.push(date)

    if(this.config.countries[1]) {
      const date2 = this.calculateOffset(date, (this.config.offsets[1]) ? this.config.offsets[1] : 0);
      const second = this.findMetric(raw, date2, this.config.countries[1], this.config.metric);
      countries.push(this.config.countries[1]);
      dates.push(date2);
      values.push(second);
    }
          
    if(this.config.countries[2]) {
      const date3 = this.calculateOffset(date, (this.config.offsets[2]) ? this.config.offsets[2] : 0);
      const third = this.findMetric(raw, date3, this.config.countries[2], this.config.metric);
      countries.push(this.config.countries[2]);
      dates.push(date3);
      values.push(third);
    }

    if(this.config.countries[3]) {
      const date4 = this.calculateOffset(date, (this.config.offsets[3]) ? this.config.offsets[3] : 0);
      const forth = this.findMetric(raw, date4, this.config.countries[3], this.config.metric);
      countries.push(this.config.countries[3]);
      dates.push(date4);
      values.push(forth);
    }

    if(this.config.countries[4]) {
      const date5 = this.calculateOffset(date, (this.config.offsets[4]) ? this.config.offsets[4] : 0);
      const fifth = this.findMetric(raw, date5, this.config.countries[4], this.config.metric);
      countries.push(this.config.countries[4]);
      dates.push(date5);
      values.push(fifth);
    }

    if(this.config.countries[5]) {
      const date6 = this.calculateOffset(date, (this.config.offsets[5]) ? this.config.offsets[5] : 0);
      const sixth = this.findMetric(raw, date6, this.config.countries[5], this.config.metric);
      dates.push(this.config.countries[5]);
      countries.push(date6);
      values.push(sixth);
    }

    if(this.config.countries[6]) {
      const date7 = this.calculateOffset(date, (this.config.offsets[6]) ? this.config.offsets[6] : 0);
      const seventh = this.findMetric(raw, date7, this.config.countries[6], this.config.metric);
      countries.push(this.config.countries[6]);
      dates.push(date7);
      values.push(seventh);
    }

    return {
      'values': values, 
      'countries': countries, 
      'dates': dates
    };
  }
  


  private isPercentMetric(): boolean {
    return this.config.metric.indexOf('variation') != -1;
  }
  
  private getMissingDates(days: string[]): string[] {
    const that = this;
    return days.reduce(function(hash){
      return function(p,c){
        var missingDaysNo = (Date.parse(c) - hash.prev) / (1000 * 3600 * 24);
        if(hash.prev && missingDaysNo > 1) {
          for(var i=1;i<missingDaysNo;i++) {
            let d = new Date(hash.prev+i*(1000 * 3600 * 24));
            p.push(that.formatDate(d));
          }
        }
        hash.prev = Date.parse(c);
        return p;
      };
    }(Object.create(null)),[]);
  }

/*
  private async loadData_legazy() {
    let data = [];

    if(this.config.countries[0]) {
      let raw = await this.eudataService.findByCountry(this.config.countries[0]).toPromise();
      if(data.length == 0) {
        data = raw.map(d => [d.date, d.new_cases > 0 ? d.new_cases : null, null, null]);
      }      
      else {
        raw.forEach(d => {
          for(let new_data of data) {
            if(d.date == new_data[0]) {
              new_data[1] = d.new_cases > 0 ? d.new_cases : null;
            }
          } 
        });
      }
    }

    if(this.config.countries[1]) {
      let raw = await this.eudataService.findByCountry(this.config.countries[1]).toPromise();
      //data.push(raw.map(d => [d.date, d.new_cases]));
      if(data.length == 0) {
        data = raw.map(d => [d.date, null, d.new_cases > 0 ? d.new_cases : null, null]);
      }      
      else {
        raw.forEach(d => {
          for(let new_data of data) {
            if(d.date == new_data[0]) {
              new_data[2] = d.new_cases > 0 ? d.new_cases : null;
            }
          } 
        });
      }
    }
      
    if(this.config.countries[2]) {
      let raw = await this.eudataService.findByCountry(this.config.countries[2]).toPromise();
      if(data.length == 0) {
        data = raw.map(d => [d.date, null, null, d.new_cases > 0 ? d.new_cases : null]);
      }
      else {
        raw.forEach(d => {
          // fil
          for(let new_data of data) {
            if(d.date == new_data[0]) {
              new_data[3] = d.new_cases > 0 ? d.new_cases : null;
            }
          } 
        });
      }
    }

    this.data = data;
  }

  */

}
