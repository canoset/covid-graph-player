import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ChartBusService } from '../../services/chart-bus.service';

@Component({
  selector: 'app-chart-options',
  templateUrl: './chart-options.component.html',
  styleUrls: ['./chart-options.component.scss'], 
  inputs:['chartId']
})
export class ChartOptionsComponent implements OnInit {
  chartId: string;

  metric: FormControl;
  graphic_type: FormControl;
  trendline: FormControl;

  start_date: FormControl;
  end_date: FormControl;
  
  showEnd: boolean;

  constructor(private busService: ChartBusService) { }

  ngOnInit(): void {
    this.metric = new FormControl();
    this.graphic_type = new FormControl();
    this.trendline = new FormControl();
    this.start_date = new FormControl();
    this.end_date = new FormControl();

    // Restore previous value or default
    const metric = localStorage.getItem('covid19.'+this.chartId+'.selected.metric')
    if(metric == null) {
      this.metric.setValue('new_cases');
    }
    else {
      this.metric.setValue(metric);
    }

    const graphType = localStorage.getItem('covid19.'+this.chartId+'.selected.graph_type');
    if(graphType == null) {
      this.graphic_type.setValue('lines');
    }
    else {
      this.graphic_type.setValue(graphType);
    }

    const trendline = localStorage.getItem('covid19.'+this.chartId+'.selected.trendline');
    if(trendline == null) {
      this.trendline.setValue('lineal');
    }
    else {
      this.trendline.setValue(trendline);
    }


    // Date
    // new Date(2020, 2, 4, 0, 0, 0, 0)
    const start_date = localStorage.getItem('covid19.'+this.chartId+'.selected.start_date');
    if(start_date == null) {
      this.start_date.setValue(new Date(2020, 2, 14, 0, 0, 0, 0));
    }
    else {
      this.start_date.setValue(new Date(start_date))
    }

    const end_date = localStorage.getItem('covid19.'+this.chartId+'.selected.end_date');
    if(end_date != null) {
      this.end_date.setValue(new Date(end_date))
    }


    this.busService.updateStartDate(this.start_date.value, this.chartId);
    this.busService.updateEndDate(this.end_date.value, this.chartId);
    this.busService.updateGraphType(this.graphic_type.value, this.chartId);
    this.busService.updateMetric(this.metric.value, this.chartId);
    this.busService.updateTrendline(this.trendline.value, this.chartId);

    

    this.metric.valueChanges.subscribe(() => {
      this.busService.updateMetric(this.metric.value, this.chartId);
      localStorage.setItem('covid19.'+this.chartId+'.selected.metric', this.metric.value);
    });

    this.graphic_type.valueChanges.subscribe(() => {
      this.busService.updateGraphType(this.graphic_type.value, this.chartId);
      localStorage.setItem('covid19.'+this.chartId+'.selected.graph_type', this.graphic_type.value);
    });

    this.trendline.valueChanges.subscribe(() => {
      this.busService.updateTrendline(this.trendline.value, this.chartId);
      localStorage.setItem('covid19.'+this.chartId+'.selected.trendline', this.trendline.value);
    });
  }

  onStartChange($event) {
    this.busService.updateStartDate(this.start_date.value, this.chartId);
    localStorage.setItem('covid19.'+this.chartId+'.selected.start_date',this.start_date.value.toJSON());
  }

  toggleLimits($event) {
    $event.preventDefault();
    this.showEnd = !this.showEnd;
  }

  onEndChange($event) {
    this.busService.updateEndDate(this.end_date.value, this.chartId);
    localStorage.setItem('covid19.'+this.chartId+'.selected.end_date',this.end_date.value.toJSON());
  }

}
