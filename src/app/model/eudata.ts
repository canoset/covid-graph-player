export class Eudata {
    id: number;
    date: Date;
    geoId: string;
    
    new_cases: number;
    new_deaths: number;
    
    total_cases: number;
    total_deaths: number;
    
    lethality: number;

    total_population: number;
    variation_cases1D: number;
    variation_deaths1D: number;
    variation_cases2D: number;
    variation_deaths2D: number;
    variation_cases4D: number;
    variation_deaths4D: number;
    variation_cases7D: number;
    variation_deaths7D: number;
    variation_cases14D: number;
    variation_deaths14D: number;
    total_deaths_per_1Mpop: number;
    total_cases_per_1Mpop: number;
}