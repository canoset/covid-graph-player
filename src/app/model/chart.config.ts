export class ChartConfig {
    metric: string;
    chart_type: string;
    trendline: string;
    chart_width: number;
    chart_height: number;
    countries: string[];
    offsets: number[];
    start_date: Date;
    end_date: Date;
}