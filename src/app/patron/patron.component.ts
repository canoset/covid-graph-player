import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-patron',
  templateUrl: './patron.component.html',
  styleUrls: ['./patron.component.scss']
})
export class PatronComponent implements OnInit {
  
  showFunding: boolean;

  constructor() { }

  ngOnInit(): void {
  }
  toggleFunding() {
    this.showFunding = !this.showFunding;
  }

}
