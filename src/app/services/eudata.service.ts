import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import { Eudata } from '../model/eudata';

@Injectable({
  providedIn: 'root'
})
export class EudataService extends BaseService {

  constructor(private http: HttpClient) { 
    super();
  }

  findByCountry(country: string): Observable<Eudata[]> {
    let url = this.getUrl(['country', country]);
    return this.http.get<Eudata[]>(url);
  }

  findByCountries(countries: string[], start_date?: Date, end_date?: Date): Observable<Map<string, Eudata[]>> {
    let url = this.getUrl(['country'], ['countries='+countries.join(',')]);
    if(start_date) {
      url += '&start_date='+start_date.getFullYear()+"-"+(start_date.getMonth()+1)+"-"+start_date.getDate();
    }
    if(end_date) {
      url += '&end_date='+end_date.getFullYear()+"-"+(end_date.getMonth()+1)+"-"+end_date.getDate();
    }
    return this.http.get<Map<string, Eudata[]>>(url);
  }
}
