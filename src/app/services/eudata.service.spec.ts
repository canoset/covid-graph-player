import { TestBed } from '@angular/core/testing';

import { EudataService } from './eudata.service';

describe('EudataService', () => {
  let service: EudataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EudataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
