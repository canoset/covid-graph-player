import { Injectable } from '@angular/core';
import { ChartBusService } from './chart-bus.service';

declare var gtag: any; 

@Injectable({
    providedIn: 'root'
})
export class TrackService {

    constructor(private busService: ChartBusService) { 
    
        this.busService.busSource$.subscribe(()=> {
            if(this.busService.googleIsReady) {
                
                gtag('event', 'charts', {
                    'event_category': 'request',
                    'event_value': 1,
                    'non_interaction': 1
                });
                
                this.busService.getConfigs()
                    .map(c => c.countries)
                    .flatMap(c => c)
                    .filter((c,index,array) => array.indexOf(c) === index)
                    .forEach(c => {
                    gtag('event', 'charts', {
                        'event_category': 'countries',
                        'event_label': c,
                        'non_interaction': 1
                    });
                });

            }
        });
        
    }



}