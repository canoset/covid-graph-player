import { environment } from '../../environments/environment';
import { HttpHeaders, HttpParams } from '@angular/common/http';

interface Options {
    headers?: HttpHeaders | {
        [header: string]: string | string[];
    };
    params?: HttpParams | {
        [param: string]: string | string[];
    };
    withCredentials?: boolean;
}

export class BaseService {
    private api_base_url: string;
    private api_version: string;

    constructor() {
        this.api_base_url = environment.API_BASE_URL;
        this.api_version = environment.API_VERSION;
    }

    public getUrl(keys: Array<any>, vars ?: Array<any>, avoidVersion ?: boolean): string {
        let url = this.api_base_url + ((avoidVersion) ? '' : this.api_version + '/') + keys.join('/');
        if (vars) {
            url += '?' + vars.join('&');
        }
        return url;
    }

    getOptions(params?: HttpParams, headers?: HttpHeaders, withCredentials: boolean = false): Options {
        return {
            params: params ? params : undefined,
            headers: headers ? headers : undefined,
            withCredentials: withCredentials
        };
    }
}
