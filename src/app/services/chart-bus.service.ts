import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { ChartConfig } from '../model/chart.config';

@Injectable({
    providedIn: 'root'
})
export class ChartBusService {

    private googleStatusSource = new Subject<any>();
    googleStatusSource$ = this.googleStatusSource.asObservable();

    googleIsReady: boolean;
    googleReady() {
        this.googleStatusSource.next('ready');
        this.googleIsReady = true;
    }

    private busSource = new Subject<any>();
    busSource$ = this.busSource.asObservable();

    // Main ChartsConfig
    private config: Map<string, ChartConfig>; 

    // lastUpdate 
    private last_chart_type: string;
    private last_trendline: string;
    private last_metric: string;
    private last_start_date: Date;
    private last_end_date: Date;
    private last_countries: string[] = [];
    private last_offsets: number[] = [];


    constructor() {
        this.config = new Map();
    }

    addChart(chartId: string) {
        const config = {
            chart_type: this.last_chart_type, 
            metric: this.last_metric,
            trendline: this.last_trendline,
            chart_width: 700,
            chart_height: 500,
            countries: this.last_countries, 
            offsets: this.last_offsets,
            start_date: this.last_start_date, 
            end_date: this.last_end_date
        };

        this.config.set(chartId, config);
    }

    updateStartDate(start_date: Date, chartId?: string) {
        this.last_start_date = start_date;
        // update all config
        this.config.forEach((e, k) => {
            if(!chartId) {
                e.start_date = this.last_start_date
            }
            else {
                if(chartId == k) {
                    e.start_date = this.last_start_date
                }
            }
        });
        this.next();
    }

    updateEndDate(end_date: Date, chartId?: string) {
        this.last_end_date = end_date;
        // update all config
        this.config.forEach( (e, k) => {
            if(!chartId) {
                e.end_date = this.last_end_date;
            }
            else {
                if(chartId == k) {
                    e.end_date = this.last_end_date;
                }
            }
        });

        this.next();
    }

    updateCountry(pos: number, alpha2Code: string, chartId?: string) {
        this.executeUpdateCountry(pos, alpha2Code, chartId);
        this.next();
    }

    

    updateCountries(alpha2Codes: string[], chartId?: string) {
        alpha2Codes.forEach( (alpha2Code, i) => {
            this.executeUpdateCountry(i, alpha2Code, chartId);
        });
        this.next();
    }

    private executeUpdateCountry(pos: number, alpha2Code: string, chartId?: string){
        if(alpha2Code == 'GB') alpha2Code = 'UK';

        // update last country
        this.last_countries[pos] = alpha2Code;
        
        // update all config
        this.config.forEach( (e, k) => {
            if(!chartId) {
                e.countries = this.last_countries
            }
            else {
                if(chartId == k) {
                    e.countries = this.last_countries;
                }
            }
        });
    }

    updateOffset(pos: number, offset: number, chartId?: string) {
        // update last country
        this.last_offsets[pos-1] = offset;
        
        // update all config
        this.config.forEach( (e, k) => {
            if(!chartId) {
                e.offsets = this.last_offsets
            }
            else {
                if(chartId == k) {
                    e.offsets = this.last_offsets;
                }
            }
        });

        
        this.next();
    }

    updateGraphType(value: any, chartId: string) {
        // update last country
        this.last_chart_type = value;
            
        // update all config
        this.config.forEach( (e, k) => {
            if(!chartId) {
                e.chart_type = this.last_chart_type
            }
            else {
                if(chartId == k) {
                    e.chart_type = this.last_chart_type;
                }
            }
        });
        
        this.next();
    }

    updateTrendline(value: any, chartId: string) {
        // update last country
        this.last_trendline = value;
            
        // update all config
        this.config.forEach( (e, k) => {
            if(!chartId) {
                e.trendline = this.last_trendline
            }
            else {
                if(chartId == k) {
                    e.trendline = this.last_trendline;
                }
            }
        });
        
        this.next();
    }


    updateMetric(value: any, chartId: string) {
        // update last country
        this.last_metric = value;
            
        // update all config
        this.config.forEach( (e, k) => {
            if(!chartId) {
                e.metric = this.last_metric
            }
            else {
                if(chartId == k) {
                    e.metric = this.last_metric;
                }
            }
        });

        this.next();
    }

    getConfig(chartId: string): ChartConfig {
        return this.config.get(chartId);
    }


    getConfigs() {
        return Array.from(this.config.values());
    }

    next() {
        this.busSource.next();
    }
} 