import { Component, OnInit } from '@angular/core';
import { ChartBusService } from './services/chart-bus.service';
import { TrackService } from './services/track.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'covid-display';

  constructor(private chartBus: ChartBusService, private trackerService: TrackService) {

  }

  ngOnInit() {
    this.chartBus.googleStatusSource$.subscribe(action => {
      if(action == 'ready') {
        this.chartBus.next();
      }
    })
  }
}
