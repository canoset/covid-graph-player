export const ENV = {
    PRODUCTION : 'prod',
    DEVEL : 'devel',
    DEVEL_STUB : 'devel-stub',
    STAGING : 'staging',
}