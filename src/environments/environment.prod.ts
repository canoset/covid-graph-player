import { ENV } from "./env.static";

export const environment = {
    production: true,
    ENV: ENV.PRODUCTION,
    API_VERSION: 'current',
    API_BASE_URL: 'http://covid19.david-canos.net:8080/',
    ASSETS_URL: '/assets/',
    OFFLINE: false
};
