import { ENV } from "./env.static";

export const environment = {
    production: false,
    ENV: ENV.DEVEL,
    API_VERSION: 'current',
    API_BASE_URL: 'http://localhost:8080/',
    ASSETS_URL: '/assets/',
    OFFLINE: false
};
