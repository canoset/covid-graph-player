import { ENV } from "./env.static";

export const environment = {
    production: false,
    ENV: ENV.STAGING,
    API_VERSION: 'current',
    API_BASE_URL: 'http://covid19-test.david-canos.net/',
    ASSETS_URL: '/assets/',
    OFFLINE: false
};
