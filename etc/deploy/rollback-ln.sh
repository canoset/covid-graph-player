#!/bin/bash

project_name=$1

ln -sfn $(readlink /var/www/html/covid19/current) /var/www/html/$project_name/rollback

echo "Rollback link created"
exit