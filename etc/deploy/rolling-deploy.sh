#!/bin/bash

remote_user=$1
data_user=$2
CI_BUILD_ID=$3
pro_backends=("david-canos.net")
project=$4

for i in $(seq 0 ${#pro_backends[@]}); do
  hostname=${pro_backends[i]}
  # stops the iterator (dunno why makes an extra loop without info)
  [ -z "$hostname" ] && continue
  
  echo "Deploying $project in hostname: ${pro_backends[i]}"
  
  # upload artifacts
  ssh $remote_user@$hostname mkdir -p /var/www/html/$project/$CI_BUILD_ID
  scp -r dist/* $remote_user@$hostname:/var/www/html/$project/$CI_BUILD_ID
  echo "Upload artifacts"
     
  # check uploaded file success
  if ssh $remote_user@$hostname stat /var/www/html/$project/$CI_BUILD_ID/index.html \> /dev/null 2\>\&1
    then
      echo "File index.html uploaded success"
    else
      echo "File index.html fail to upload"
      exit 1
  fi

  # deployment
  #ssh $remote_user@$hostname /home/$remote_user/scripts/$project/rollback-ln.sh $project
  ssh $remote_user@$hostname ln -sfn /var/www/html/$project/$CI_BUILD_ID /var/www/html/$project/current

done

exit
